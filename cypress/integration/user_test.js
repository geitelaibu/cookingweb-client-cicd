/* eslint-disable */
describe('User page Test', function () {
  beforeEach(() => {
    cy.visit('http://localhost:8080/?#/user')
  })
  it('visit user recipe page and check basic component:', function () {
    cy.url().should('have', 'user')
    cy.get('span').should('contain','Profile')

    cy.get('#1')
    cy.get('#2')
    cy.get('#3')
    cy.get('#4')

  })

  it('Checking button:', function () {
    cy.url().should('have', 'write')
    cy.get('#1').click()
    cy.get('#2').click()
    cy.get('#3').click()
    cy.get('#4').click()
  })

  it('Checking Profile', function () {

    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('have', 'login')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('starstar')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.wait(500)
    cy.url().should('include', 'community')
    cy.visit('http://localhost:8080/?#/user')

    cy.get('#4').click()
    cy.get('#profile')
    cy.get('h3').should('contain','John')
  })

  it('Checking change password:', function () {
    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('have', 'login')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('starstar')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.wait(500)
    cy.url().should('include', 'community')
    cy.visit('http://localhost:8080/?#/user')

    cy.get('#1').click()

    cy.get('.el-button')
    cy.get('.el-input')

    cy.get("#newPassword").type('54321')
    cy.get("#confirmPassword").type('54321')
    cy.get('#newPassword').click()
    cy.get('.el-button').click()

    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('have', 'login')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('starstar')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.url().should('include', 'login')
  })

  it('Checking edit information:', function () {
    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('have', 'login')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('54321')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.url().should('include', 'community')
    cy.visit('http://localhost:8080/?#/user')

    cy.get('#2').click()

    cy.get('.el-button')
    cy.get('.el-input')

    cy.get('#personal').type('changed message')
    cy.get('#change').click()

    cy.get('#4').click()
    cy.get('#profile')
    //cy.get('#info').should('contain','changed message')
  })

  it('Checking remove user:', function () {
    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('have', 'login')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('54321')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.url().should('include', 'community')
    cy.visit('http://localhost:8080/?#/user')
    cy.get('#3').click()

    cy.get('.el-button').click()

    cy.get('#4').click()
    cy.get('#profile')

    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('have', 'login')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('54321')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.url().should('include', 'login')
  })
})
