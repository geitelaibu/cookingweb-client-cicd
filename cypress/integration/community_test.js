/* eslint-disable */
const apiURL = "https://cookingweb-api-staging.herokuapp.com/recipes";

const recipe = {
  name: 'Dumpling',
  username: 'Francis',
  type: ['Chinese food', 'lunch'],
  ingredients: ['pork', 'flour', 'vegetable'],
  process: ['Mincing pork', 'Kneading dough'],
  comment: [],
  like: 0}

describe('Community test', function () {
  describe('Test Community page', function () {
    beforeEach(() => {
      cy.visit('http://localhost:8080/?#/community')
      cy.wait(500)
    })
    it('visit community page and check component:', function () {
      cy.url().should('have', 'community')
      cy.get('.vue-title')
      cy.get('#app1')
      cy.get('.el-carousel')
      cy.get('.time')
      cy.get('.bottom')
      cy.get('.button')
      cy.get('#type1')
      cy.get('#ingredients1')
      cy.get('#process1')
      cy.get('#comment1')
    })

    it('Test click element', function () {
      cy.url().should('have', 'community')
      cy.get('.button').click({multiple: true})
    })
  })

  describe("Test the page changed with a new recipe added", () => {
    beforeEach(() => {
      cy.request("POST", apiURL, recipe);
      cy.visit('http://localhost:8080/?#/community')
    })
    it("add recipe by 1", () => {
      cy.get("tbody")
        .find("tr")
        .should("have.length", 6);


    })
  })
})

