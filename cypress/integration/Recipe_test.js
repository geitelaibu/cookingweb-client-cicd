/* eslint-disable */
describe('Recipe Test', function () {
  describe('Test Manage Recipe Page', function () {
    beforeEach(() => {
      cy.visit('http://localhost:8080/?#/recipes')
      cy.wait(500)
    })
    it('visit Recipe page and check component:', function () {
      cy.url().should('have', 'recipes')
      cy.get('.vue-title')
      cy.get('#app1')
      cy.get('span')
      cy.get('h1')
      cy.get('.el-divider')
      cy.get('button')
    })

    it('Checking the page after logging', function () {
      cy.url().should('have', 'recipes')
      cy.visit('http://localhost:8080/?#/login')
      cy.url().should('include','login')
      cy.get('.loginPage')
        .should('contain','Login')
      cy.get('#user')
        .type('Francis')
      cy.get('#password')
        .type('123456789')
      cy.get('#user').click()
      cy.get('#loginButton').click()
      cy.visit('http://localhost:8080/?#/recipes')
      cy.get('.clearfix')
    })
  })

  describe('Test functionality', function () {
    beforeEach(() => {
      //login with Francis
      cy.visit('http://localhost:8080/?#/login')
      cy.url().should('include','login')
      cy.get('.loginPage')
        .should('contain','Login')
      cy.get('#user')
        .type('Francis')
      cy.get('#password')
        .type('123456789')
      cy.get('#user').click()
      cy.get('#loginButton').click()
      cy.visit('http://localhost:8080/?#/recipes')
    })

    it('Test edit', function () {
      cy.get("tbody")
        .find("tr")
        .should("have.length", 3);
      // Click trash/delete link of 3rd donation in list
      cy.get('tbody')
        .find('tr')
        .eq(2)
        .find('td')
        .eq(9)
        .find('a')
        .click()
      cy.url().should('contain', 'edit')
    })

    it('Test delete successful', function () {
      cy.get("tbody")
        .find("tr")
        .should("have.length", 3);
      // Click trash/delete link of 3rd donation in list
      cy.get('tbody')
        .find('tr')
        .eq(2)
        .find('td')
        .eq(10)
        .find('a')
        .click()
      // Click confirmation button
      cy.get('button')
        .contains('Delete')
        .click()
      cy.get('tbody')
        .find('tr')
        .should('have.length', 2)
    })
    it("Test delete cancel", () => {
      cy.get("tbody")
        .find("tr")
        .should("have.length", 2);
      // Click trash/delete link of 2rd donation in list
      cy.get("tbody")
        .find("tr")
        .eq(1)
        .find("td")
        .eq(10)
        .find("a")
        .click();
      // Click cancel button
      cy.get("button")
        .contains("Cancel")
        .click();
      cy.get("tbody")
        .find("tr")
        .should("have.length", 2);
    });
  })
})
