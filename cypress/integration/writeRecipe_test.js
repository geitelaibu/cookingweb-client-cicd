/* eslint-disable */
describe('Write Recipe Test', function () {
  beforeEach(() => {
    cy.visit('http://localhost:8080/?#/write')
  })
  it('visit write recipe page and check component:', function () {
    cy.url().should('have', 'write')
    cy.get('.vue-title')
    cy.get('#app1')
    cy.get('.el-button')
    cy.get('.writePage')
    cy.get('.el-input')
    cy.get('#btn1')
    cy.get('#btn2')
    cy.get('#btn3')
    cy.get('#submit')
    cy.get('#reset')
    cy.get('#title')

  })

  it('Checking button:',function () {
    cy.url().should('have', 'write')
    cy.get('#btn1').click()
    cy.get('#btn2').click()
    cy.get('#btn3').click()
  })

  it('Test write a recipe', function () {
    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('include','login')
    cy.get('.loginPage')
      .should('contain','Login')
    cy.get('#user')
      .type('Francis')
    cy.get('#password')
      .type('123456789')
    cy.get('#user').click()
    cy.get('#loginButton').click()

    cy.visit('http://localhost:8080/?#/write')

    // cy.get('#title').type('MushroomSoup')
    // cy.get('#btn1').click()
    // cy.get('#input1').type('soup')
    // cy.get('#btn2').click()
    // cy.get('#input2').type('mushroom')
    // cy.get('#btn3').click()
    // cy.get('#input3').type('boil')
    // cy.get('#title').click()
    // cy.get('#submit').click()
    // cy.visit('http://localhost:8080/?#/community')
    // cy.get('div').should('contain', 'mushroom')
  })

  it('Test reset button', function () {
    cy.visit('http://localhost:8080/?#/login')
    cy.url().should('include','login')
    cy.get('.loginPage')
      .should('contain','Login')
    cy.get('#user')
      .type('Francis')
    cy.get('#password')
      .type('123456789')
    cy.get('#user').click()
    cy.get('#loginButton').click()

    cy.visit('http://localhost:8080/?#/write')

    // cy.get('#title').type('mushroomSoup')
    // cy.get('#btn1').click()
    // cy.get('#input1').type('soup')
    // cy.get('#btn2').click()
    // cy.get('#input2').type('mushroom')
    // cy.get('#btn3').click()
    // cy.get('#input3').type('boil')
    //
    // cy.get('#reset').click()
    // cy.get('#title').should('have.value', '')

  })

})
