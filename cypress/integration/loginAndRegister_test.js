/* eslint-disable */
describe('Login Test and Register Test', function () {
  it('visit Login page and check component:', function () {
    cy.visit('http://localhost:8080/?#/login')
    cy.wait(500)
    cy.get('#user')
      .type('Francis')
      .should('have.value', 'Francis')
    cy.get('#password')
      .type('123456789')
      .should('have.value', '123456789')
  })
  it('login as Francis:',function(){
    const user = {
        name:'Francis',
        password:'123456789'
    }
    cy.request({
      url: 'http://localhost:8080/?#/login',
      method: 'GET',
      form: true,
      body: user
    })
    cy.visit('http://localhost:8080/?#/login')
    cy.wait(500)
    cy.url().should('include','login')
    cy.get('.loginPage')
      .should('contain','Login')
    cy.get('#user')
      .type('Francis')
    cy.get('#password')
      .type('123456789')
    cy.get('#user').click()
    cy.get('#loginButton').click()
    cy.url().should('include', 'community')
  })

  it('visit Register page and check component:', function () {
    cy.visit('http://localhost:8080/?#/register')
    cy.wait(500)
    cy.get('#user')
      .type('Francis')
      .should('have.value', 'Francis')
    cy.get('#password')
      .type('123456789')
      .should('have.value', '123456789')
    cy.get('#confirmPassword')
      .type('123456789')
      .should('have.value', '123456789')
    cy.get('#registerButton')
    cy.get('#resetButton')
  })

  it('Register as John:',function(){
    const user = {
      name: "John",
      password: "starstar",
      sex: "male",
      personal_lnfo: "Hello"
    }
    it("should return confirmation message and add a user", function () {
      return request(server)
        .post("/users")
        .send(user)
        .expect(200)
        .then(res => {
          expect(res.body).to.have.property("message", "User Added Successfully!")
        })
    })
    cy.visit('http://localhost:8080/?#/register')
    cy.url().should('include','register')
    cy.get('.registerPage')
      .should('contain','Register')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('starstar')
    cy.get('#confirmPassword')
      .type('starstar')
    cy.get('#user').click()
    cy.get('#registerButton').click()
    cy.wait(500)
    cy.url().should('include', 'community')
  })

  it('Test username is already used:',function(){
    cy.visit('http://localhost:8080/?#/register')
    cy.wait(500)
    cy.url().should('include','register')
    cy.get('.registerPage')
      .should('contain','Register')
    cy.get('#user')
      .type('John')
    cy.get('#password')
      .type('starstar')
    cy.get('#confirmPassword')
      .type('starstar')
    cy.get('#user').click()
    cy.get('#registerButton').click()
    cy.url().should('include', 'register')
  })

  it('Test Reset Button', function () {
    cy.visit('http://localhost:8080/?#/register')
    cy.wait(500)
    cy.get('#user')

    cy.get('#password')

    cy.get('#confirmPassword')

    cy.get('#resetButton').click()
    cy.get('#user').should('have.value', '')
    cy.get('#password').should('have.value', '')
    cy.get('#confirmPassword').should('have.value', '')
  })

})
