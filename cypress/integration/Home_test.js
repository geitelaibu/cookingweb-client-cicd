/* eslint-disable */
describe('Home Test', function () {
  beforeEach(() => {
    cy.visit('http://localhost:8080/?#/')
  })

  describe("Test Navigation bar", () => {
    it("Shows the required links", () => {
      cy.get(".navbar-nav")
        .eq(0)
        .within(() => {
          cy.get(".nav-item")
            .eq(0)
            .should("contain", "Community");
          cy.get(".nav-item")
            .eq(1)
            .should("contain", "Write Recipes");
          cy.get(".nav-item")
            .eq(2)
            .should("contain", "Manage Recipes");
        })
      // cy.get(".nav-form")
      //   .eq(0)
      //   .within(() => {
      //     cy.get(".form-input")
      //     cy.get(".button")
      //   })
      cy.get(".navbar-nav")
        .eq(1)
        .within(() => {
          cy.get(".nav-item")
            .eq(0)
            .should("contain", "About Us");
        })
    })

    it("Redirects when links are clicked", () => {
      cy.get(".navbar-nav")
        .eq(0)
        .find(".nav-item")
        .eq(0)
        .click();
      cy.url().should("include", "/community");
      cy.get(".navbar-nav")
        .eq(0)
        .find(".nav-item")
        .eq(1)
        .click();
      cy.url().should("include", "/write");
      cy.get(".navbar-nav")
        .eq(0)
        .find(".nav-item")
        .eq(2)
        .click();
      cy.url().should("include", "/recipes");
      cy.get(".navbar-nav")
        .eq(1)
        .find(".nav-item")
        .eq(0)
        .click();
      cy.url().should("include", "/about");
    });
  })


  describe("Test Home Page", () => {
    it('visit Home page and check login component:', function () {
      cy.get('#login').click()
      cy.url().should('have', 'login')
    })
    it('visit Home page and check register component:', function () {
      cy.get('#register').click()
      cy.url().should('have', 'login')
    })
    it('visit Home page and check community component:', function () {
      cy.get('#community').click()
      cy.url().should('have', 'login')
    })
    it('visit Home page and check text:', function () {
      cy.get('h1')
        .should('contain', 'COOKING')
    })
    it('visit Home page and check redBlock:', function () {
      cy.get('.redblock')
    })
    it('visit Home page and check blackBlock:', function () {
      cy.get('.blackblock')
    })
  })

})
