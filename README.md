# Assignment 2 - Agile Software Practice.

Name: Yan Hu

Number: 20086446

## Client UI.

Show some screenshots of each view/page in Vue app - include a short caption stating the purpose of each one.

![](src/assets/screenshots/home.png)

>>The homepage, users can go to login, register and community page

![](src/assets/screenshots/login.png)

>>Users can login in this page

![](src/assets/screenshots/register.png)

>>Users can register in this page

![](src/assets/screenshots/community.png)

>>This page includes all recipes in database, user can comment and click like

![](src/assets/screenshots/write.png)

>>Allows the user add a new recipe

![](src/assets/screenshots/manage.png)

>>Allows users manage their recipe, including edit information, delete comment and so on.

![](src/assets/screenshots/user.png)

>>Users in this page can look their profile and change password or information if they want.

![](src/assets/screenshots/search.png)

>>Allows the user search recipes by name (implement by fuzzy search), type 'i', the result show 'Pizza', 'Chips'.

## E2E/Cypress testing.
non-standard features
```
cy.request({
      url: 'http://localhost:8080/?#/login',
      method: 'GET',
      form: true,
      body: user
    })
```
````
cy.get('.button').click({multiple: true})
````
{multiple: true} can click more than one button at the same time.
## Web API CI.

the GitLab Pages URL of the coverage report for Web API tests.

https://geitelaibu.gitlab.io/cookingweb-cicd/coverage/lcov-report/


