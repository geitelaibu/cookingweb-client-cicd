/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true,
  state: {
    userState: 'User',
    recipeName: ''
  },
  mutations: {
    loginState (state, name) {
      state.userState = name
    },

    registerState (state, name) {
      state.userState = name
    },

    signOutState (state) {
      state.userState = 'User'
    },

    searchName (state, name) {
      state.recipeName = name
    }

  }
})

export default store
