import Api from '@/services/api'

export default {
  fetchRecipes () {
    return Api().get('/recipes')
  },

  fetchRecipesByName (username) {
    return Api().get(`/recipes/username/${username}`)
  },
  fetchRecipeByID (id) {
    return Api().get(`/recipes/${id}`)
  },
  getComments (id) {
    return Api().get(`/recipes/${id}/comment`)
  },
  search (name) {
    return Api().get(`/recipes/search/${name}`)
  },
  postRecipe (recipe) {
    return Api().post('/recipes', recipe,
      { headers: {'Content-type': 'application/json'} })
  },
  postComment (id, comment) {
    return Api().post(`/recipes/${id}/addComment`, comment,
      { headers: {'Content-type': 'application/json'} })
  },
  upLike (id) {
    return Api().put(`recipes/${id}/upLike`)
  },
  editRecipe (id, recipe) {
    return Api().put(`/recipes/${id}/editRecipes`, recipe,
      { headers: {'Content-type': 'application/json'} })
  },
  deleteRecipe (id) {
    return Api().delete(`/recipes/${id}`)
  },
  deleteComment (id, cid) {
    return Api().delete(`/recipes/${id}/comment/${cid}`)
  }
}
