import Api from '@/services/api'

export default {
  fetchUsers () {
    return Api().get('/users')
  },

  fetchUser (name) {
    return Api().get(`/users/name/${name}`)
  },

  postUser (user) {
    return Api().post('/users', user,
      { headers: {'Content-type': 'application/json'} })
  },

  changePassword (id, user) {
    return Api().put(`/users/${id}/password`, user,
      { headers: {'Content-type': 'application/json'} })
  },

  editInfo (id, user) {
    return Api().put(`/users/${id}/editInformation`, user,
      { headers: {'Content-type': 'application/json'} })
  },

  deleteUser (id) {
    return Api().delete(`/users/${id}`)
  }
}
