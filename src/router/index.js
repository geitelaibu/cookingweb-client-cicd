import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Community from '@/components/Community'
import Write from '@/components/WriteRecipe'
import Recipes from '@/components/Recipes'
import AboutUs from '@/components/AboutUs'
import User from '@/components/User'
import Edit from '@/components/Edit'
import Result from '@/components/SearchResult'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/community',
      name: 'Community',
      component: Community
    },
    {
      path: '/write',
      name: 'WriteRecipe',
      component: Write
    },
    {
      path: '/recipes',
      name: 'Recipes',
      component: Recipes
    },
    {
      path: '/about',
      name: 'AboutUs',
      component: AboutUs
    },
    {
      path: '/user',
      name: 'User',
      component: User
    },
    {
      path: '/edit',
      name: 'Edit',
      component: Edit,
      props: true
    },
    {
      path: '/result',
      name: 'SearchResult',
      component: Result
    }
  ]
})
